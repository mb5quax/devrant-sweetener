# devrant-sweetener
On the devRant web client, the background of the rants use the ranter's background color. When using the dark theme, this behavior can strain the viewer's eye if the color is too bright. This userscript aims to reduce the colored zone to reduce eye strain.

So what does it actually do ? It sets the rant background to the main dark
theme color and rounds the ranter's avatar so it's background color is still
visible, just more discreet.
If the ranter hasn't yet an avatar, it creates a dummy one featuring the
ranter's username.

Currently, it supports only the dark theme. If you want it for the clear theme,
hit me up, I'll try to add support as well.

# Table of contents
* [Screenshots](#screenshots)
  * [Avatar](#avatar)
  * [No avatar](#no-avatar)
* [Download](#download)
* [Disclaimer](#disclaimer)

# Screenshots
## Avatar
![image](screenshot.jpg)
## No avatar
![image](screenshot-no-avatar.jpg)

# Download
Download tampermonkey:

* **Chrome:** https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo
* **Firefox:** https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/
* **Opera:** https://addons.opera.com/en/extensions/details/tampermonkey-beta/
* **Other:** https://tampermonkey.net/ (follow instructions there)

Then install the user-script here: https://gitlab.com/mb5quax/devrant-sweetener/raw/master/devrant-background-sweetener.js
*NB : If Tampermonkey doesn't open when you click on that link, copy-paste the
raw code into a new userscript from Tampermonkey dashboard, and copy-paste the
URL in the "Update from URL" field in the script parameters.*

# Disclaimer
*This readme is based on [7twin](https://github.com/7twin/ "7twin's GitHub page") readme files. There's some nice devRant userscripts over there. Go check them out !*
