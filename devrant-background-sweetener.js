// ==UserScript==
// @name         DevRant Background Sweetener
// @namespace    https://devrant.com/
// @version      0.1
// @description  Render the ranter's background color only in their avatar
// @author       mb5quax
// @match        https://devrant.com/rants/*
// @grant        none
// @run-at       document-end
// ==/UserScript==

(function() {
	'use strict';
	let mainColor = "#40415A";
	let banner = document.getElementsByClassName("rant-banner")[0];
	let notifBadge = document.getElementsByClassName("notif-badge")[0];
	let avatarDiv = document.getElementsByClassName("rant-avatar-scroll")[0];
	let avatar = avatarDiv.getElementsByTagName("img")[0];
	let navbar = document.getElementsByClassName("nav scroll")[0];
	let logoIcons = navbar.getElementsByClassName("logo-icon")[0].children;

	/* if avatar doesn't exists, create an empty one */
	if(avatar === undefined) {
		let ranterName = document.getElementsByClassName("rant-username")[0].text;
		let ranterColor = document.getElementsByClassName("rant-banner")[0].style.backgroundColor;
		if(ranterColor[0] !== '#') {
			ranterColor = rgb2hex(ranterColor);
		}
		avatar = document.createElement("img");
		avatar.src = "https://ipsumimage.appspot.com/1400?l=" + ranterName + "&s=200&b=" + ranterColor.substring(1) + "&f=ffffff&t=png";
		avatarDiv.appendChild(avatar);
	}

	/* style changes, go ! */
	// banner
	banner.style.backgroundColor = mainColor;
	// avatar
	avatar.style.borderRadius = "50%";
	// notif badge
	notifBadge.style.borderColor = mainColor;
	// navbar
	function colourThatLogoBitches(){
		logoIcons[0].style.opacity = 1;
		logoIcons[1].style.opacity = 1;
		logoIcons[2].style.opacity = 0;
	};

	colourThatLogoBitches();
	window.addEventListener("scroll", colourThatLogoBitches);

	// converts rgb[a] value to hex (thanks to Zack Katz on StackOverflow
	// See : https://stackoverflow.com/a/3971432
	// used because sometimes browsers return rgba values instead of hex
	function rgb2hex(rgb) {
		rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
		function hex(x) {
			return ("0" + parseInt(x).toString(16)).slice(-2);
		}
		return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
	}
})();
